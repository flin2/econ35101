/*
cd ~/Dropbox/Chicago/ECON35101_Dingel/HW02/
bash zbash_do.sh "prep_ssc_install.do" "" ""
*/

ssc install reghdfe
ssc install ftools
ssc install ppml
ssc install poi2hdfe
ssc install ppmlhdfe
ssc install hdfe
ssc install estout
ssc install heatplot
ssc install palettes
ssc install colrspace
ssc install gtools
ssc install regsave

// End of do file