#!/bin/bash

# bash zbash_do.sh "" "" ""

script=$1
log=$2
param=$3
sys_os=mac
dir_proj="~/Users/flin/Dropbox/Chicago/ECON35101_Dingel/"

export script sys_os dir_proj

# If ${log} is empty, use default log file
if [ -z "${log}" ]
then
    log="${script%.do}.log"
fi

# Run script
stata-mp -b ${script} ${param}

mv ${script%.do}.log ${log}

# End of bash script