/*
cd ~/Dropbox/Chicago/ECON35101_Dingel/HW02/
bash zbash_do.sh "HW02_Table_Stata.do"
*/

set more off
clear all
set maxvar 20000
set matsize 11000
set linesize 255

global sys_user = c(username)
global sys_os: env sys_os
global sys_batch = c(mode)=="batch"

di "Started at: $S_TIME  $S_DATE"
global rev_date: di %tdCYND date(c(current_date), "DMY")
global dir_proj = "~/Dropbox/Chicago/ECON35101_Dingel/HW02/"
cd "${dir_proj}"

global dir_data = "./data/"
global dir_out = "./output/"
capture noi mkdir "${dir_out}"

/*==============================================================================
Import data
*/

global ds_raw = "${dir_data}/Detroit"
import delimited "${dir_data}/Detroit.csv", clear
gen lflows = ln(flows)
gen ldist = ln(distance_google_miles)
gen lmins = ln(duration_minutes)
gen dist = distance_google_miles
gen mins = duration_minutes
label var lflows "ln(flow)"
label var ldist "ln(Google distance)"
label var lmins "ln(Google time)"
label var dist "Google distance"
label var mins "Google time"
save "${ds_raw}", replace

/*==============================================================================
Program that runs regression, stores output and runtime
*/

capture program drop f_reg
program f_reg
	args i_reg code_reg
	
	timer on `i_reg'
	eststo est`i_reg': `code_reg'
	timer off `i_reg'
	
	qui timer list
	estadd local time = r(t`i_reg')
end


/*==============================================================================
Table 1: Log-linear estimation
*/

use "${ds_raw}", clear

keep if flows > 0 // use only non-zero commuters

/*------------------------------------------------------------------------------
log bilateral (Google driving) distance
*/

timer clear
eststo clear

global x = "ldist"

*-----------------------------------------------------------
* reg

f_reg 1 "qui reg lflows ${x} i.home_id i.work_id"
est table, keep(${x}) b se

*-----------------------------------------------------------
* xtreg

xtset, clear
xtset home_id // Absorb home_id
f_reg 2 "qui xtreg lflows ${x} i.work_id, fe"
est table, keep(${x}) b se
xtset, clear

*-----------------------------------------------------------
* areg

f_reg 3 "qui areg lflows ${x} i.work_id, absorb(home_id)" // Absorb home_id
est table, keep(${x}) b se

*-----------------------------------------------------------
* reghdfe

f_reg 4 "reghdfe lflows ${x}, absorb(home_id work_id)"

*-----------------------------------------------------------
* Export table panel

esttab using ${dir_out}/Table1_PanelA.tex, replace label booktabs se r2 ///
	keep(${x}) mtitles("reg" "xtreg" "areg" "reghdfe") ///
	scalars("time Time") sfmt(%8.3f)

/*------------------------------------------------------------------------------
log bilateral (Google driving) time
*/

timer clear
eststo clear

global x = "lmins"

*-----------------------------------------------------------
* reg

f_reg 1 "qui reg lflows ${x} i.home_id i.work_id"
est table, keep(${x}) b se

*-----------------------------------------------------------
* xtreg

xtset, clear
xtset home_id // Absorb home_id
f_reg 2 "qui xtreg lflows ${x} i.work_id, fe"
est table, keep(${x}) b se
xtset, clear

*-----------------------------------------------------------
* areg

f_reg 3 "qui areg lflows ${x} i.work_id, absorb(home_id)" // Absorb home_id
est table, keep(${x}) b se

*-----------------------------------------------------------
* reghdfe

f_reg 4 "reghdfe lflows ${x}, absorb(home_id work_id)"

*-----------------------------------------------------------
* Export table panel

esttab using ${dir_out}/Table1_PanelB.tex, replace label booktabs se r2 ///
	keep(${x}) mtitles("reg" "xtreg" "areg" "reghdfe") ///
	scalars("time Time") sfmt(%8.3f)

/*==============================================================================
Table 2: Zeros
*/

timer clear
eststo clear

use "${ds_raw}", clear

* Function that runs tests and prepare variables
capture program drop f_hettest
program f_hettest
	args i_reg
	
	estat hettest
	estadd local het_chi2 = round(r(chi2))
	estadd local het_p = r(p)
	
	* Prepare variables for scatter plot
	predict lflows_pred`i_reg'
	gen lflows_res`i_reg' = lflows - lflows_pred`i_reg' if flows != 0
end

/*------------------------------------------------------------------------------
For 1-5, x is ldist
For 6-8, x is ldist (according to THE LOG OF GRAVITY paper)
*/

global x = "ldist"

*-----------------------------------------------------------
* 1. A log-linear regression that omits observations in which flow equals zero.

f_reg 1 "qui reg lflows ${x} i.home_id i.work_id if flows > 0"
f_hettest 1
est table, keep(${x}) b se

*-----------------------------------------------------------
* 2. A log-linear regression that omits observations in which flow equals zero. In addition, set the dependent variable to log of flow plus one.

gen lflows_p1 = ln(flows+1)
f_reg 2 "qui reg lflows_p1 ${x} i.home_id i.work_id if flows > 0"
f_hettest 2
est table, keep(${x}) b se

*-----------------------------------------------------------
* 3. A log-linear regression in which the dependent variable is log of flow plus one.

f_reg 3 "qui reg lflows_p1 ${x} i.home_id i.work_id"
f_hettest 3
est table, keep(${x}) b se

*-----------------------------------------------------------
* 4. A log-linear regression in which the dependent variable is log of flow plus 0.01.

gen lflows_p001 = ln(flows+0.01)
f_reg 4 "qui reg lflows_p001 ${x} i.home_id i.work_id"
f_hettest 4
est table, keep(${x}) b se

*-----------------------------------------------------------
* 5. A log-linear regression in which the dependent variable is log flow, but flows Xij that are zero in the data are replaced by the value log(1e-12 times Xjj) as the dependent variable.

preserve
keep if home_id == work_id
gen lflows_l12 = -12*ln(10) + ln(flows) // There are still 0s
keep work_id lflows_l12
save "${dir_data}/temp_jj.dta", replace
restore 
merge m:1 work_id using "${dir_data}/temp_jj.dta", assert(match) nogen

gen lflows_spec5 = ln(flows)
replace lflows_spec5 = lflows_l12 if flows == 0
f_reg 5 "qui reg lflows_spec5 ${x} i.home_id i.work_id"
f_hettest 5
est table, keep(${x}) b se

save "${dir_data}/table2_scatter", replace

/*------------------------------------------------------------------------------
Scatterplot of the residuals
*/

use "${dir_data}/table2_scatter", clear
egen id = group(home_id work_id)
keep id ldist lflows_res*
reshape long lflows_res, i(id ldist) j(spec)
label var lflows "ln(flows) Residuals"
label var spec "Specification"

twoway (scatter lflows_res ${x}, msize(tiny) note("")), by(spec,yr) bgcolor(white)
graph save "${dir_out}/Table2_het_scat", replace
* graph export "${dir_out}/Table2_het_scat.pdf", replace // Too large

heatplot lflows ${x}, by(spec,yr legend(off) note("")) bwidth(0.1) ///
	colors(spmap) scheme(s1color)
graph save "${dir_out}/Table2_het_heat", replace
graph export "${dir_out}/Table2_het_heat.pdf", replace

esttab using ${dir_out}/Table2_het.tex, replace label booktabs noobs ///
	drop(*) mtitles("reg 1" "reg 2" "reg 3" "reg 4" "reg 5") ///
	scalars("het_chi2 Breusch–Pagan $\chi^2$" "het_p Breusch–Pagan p-value") sfmt(%9.3g)

/*------------------------------------------------------------------------------
For 1-5, x is ldist
For 6-8, x is ldist (according to THE LOG OF GRAVITY paper)
*/

use "${ds_raw}", clear
global x = "ldist"

*-----------------------------------------------------------
* 6. An estimate of the same constant-elasticity specification that uses the poi2hdfe command to implement the PPML estimator of Guimaraes, Figueirdo, and Woodward (REStat 2003) and Silva and Tenreyro (REStat 2006). Use all observations, including zeros, in this and the next column.

f_reg 6 "poi2hdfe flows ${x}, id1(home_id) id2(work_id)"

*-----------------------------------------------------------
* 7. An estimate of the same constant-elasticity specification that uses the ppmlhdfe command to implement the PPML estimator.

f_reg 7 "ppmlhdfe flows ${x}, absorb(home_id work_id)"

*-----------------------------------------------------------
* 8. An estimate of the constant-elasticity specification that uses the ppmlhdfe command to implement the PPML estimator. Omit observations in which flow equals zero.

f_reg 8 "ppmlhdfe flows ${x} if flows > 0, absorb(home_id work_id)"


esttab using ${dir_out}/Table2.tex, replace label booktabs se r2 ///
	keep(ldist) mtitles("reg 1" "reg 2" "reg 3" "reg 4" "reg 5" "poi2hdfe" "ppmlhdfe" "ppmlhdfe non-0") ///
	scalars("time Time")  sfmt(%8.3f)

/*==============================================================================
Table 3: Horse Race
*/


timer clear
eststo clear

global x = "ldist"

use "${ds_raw}", clear

f_reg 1 "reghdfe lflows ${x} if flows > 0, absorb(home_id work_id) vce(r)"
esttab using ${dir_out}/Table3_Stata.tex, replace label booktabs se r2 ///
	keep(${x}) mtitles("reghdfe") ///
	scalars("time Time") sfmt(%8.3f)
	
* Also save the main results to a file in case we want to generate a table together
regsave ${x}, tstat pval cmdline
gen time = e(time)
save ${dir_out}/Table3_Stata, replace

di "Ended at: $S_TIME  $S_DATE"
* End of do file
