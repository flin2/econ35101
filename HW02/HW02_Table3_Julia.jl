# ECON 35101 International Macroeconomics and Trade Assignment 2
# Feng Lin
# cd ~/Dropbox/Chicago/ECON35101_Dingel/HW02/
# bash zbash_jl.sh "HW02_Table3_Julia.jl" "" ""

@show ARGS

dir_proj = "/Users/flin/Dropbox/Chicago/ECON35101_Dingel/HW02/" # Change to the directory where the zip file is unzipped
cd(dir_proj)

dir_data = "./data/"
dir_out = "./output/"

using Pkg,Dates
if false # When running the code for the first time, change to true
    Pkg.add(["Random","RCall"])
    Pkg.add(["LinearAlgebra","Optim","BlackBoxOptim"])
    Pkg.add(["DataFrames","DataFramesMeta","Chain","ShiftedArrays","CSV","StatFiles"])
    Pkg.add(["Plots","Gadfly","Compose","Cairo","Fontconfig"])
    Pkg.add(["DelimitedFiles"])
    Pkg.add(["FixedEffectModels","Vcov","RegressionTables"])
end
Pkg.instantiate()

using Random,RCall
using LinearAlgebra,Optim,BlackBoxOptim
using DataFrames,DataFramesMeta,Chain,ShiftedArrays,CSV,StatFiles
using Plots,Gadfly,Compose,Cairo,Fontconfig
using DelimitedFiles
using FixedEffectModels,Vcov,RegressionTables

println(string("Started at ",Dates.format(now(), "yyyy-mm-dd HH:MM:SS\n")))

################################################################################

#df_raw = DataFrame(CSV.File(dir_data * "Detroit.csv"))
#@transform!(df_raw, :lflows = log.(:flows))

df_raw = DataFrame(load(dir_data * "Detroit.dta"))

function f_regtime(regtime_out)
    reg_out,time_out = regtime_out[1],regtime_out[2];
    println(reg_out)
    println(string("Run Time: ",time_out))
    return reg_out,time_out
end

println("Compilation Round")
regtime_out = @timed reg(@subset(df_raw, :flows .> 0), @formula(lflows ~ ldist + fe(home_id) + fe(work_id)), Vcov.robust())
reg_out1,time_out1 = f_regtime(regtime_out);

println("Performance Round")
regtime_out = @timed reg(@subset(df_raw, :flows .> 0), @formula(lflows ~ ldist + fe(home_id) + fe(work_id)), Vcov.robust())
reg_out2,time_out2 = f_regtime(regtime_out);

stats_out = (Time = [time_out1,time_out2],)
regtable(reg_out1,reg_out2; renderSettings = latexOutput(dir_out*"Table3_Julia.tex"),custom_statistics = stats_out)

# Also save the main results to a file in case we want to generate a table together
df_out = DataFrame(id=Int64[], x=String[], b=Float64[], se=Float64[], t=Float64[], p=Float64[], n=Int64[], r2=Float64[], time=Float64[])

reg_out = reg_out1
time_out = time_out1
push!(df_out,[1 coefnames(reg_out) coef(reg_out) stderror(reg_out) coeftable(reg_out).cols[3] coeftable(reg_out).cols[4] nobs((reg_out)) r2(reg_out) time_out])
reg_out = reg_out2
time_out = time_out2
push!(df_out,[2 coefnames(reg_out) coef(reg_out) stderror(reg_out) coeftable(reg_out).cols[3] coeftable(reg_out).cols[4] nobs((reg_out)) r2(reg_out) time_out])

R"saveRDS($df_out, paste0($dir_out,'Table3_Julia.rds'))"

println(string("Ended at ",Dates.format(now(), "yyyy-mm-dd HH:MM:SS")))
# End of Julia script