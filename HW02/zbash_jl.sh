#!/bin/bash

# bash zbash_jl.sh "" "" ""

script=$1
log=$2
param=$3

export script

# If ${log} is empty, use default log file
if [ -z "${log}" ]
then
    log="${script%.jl}.log"
fi

# run Rscript
julia  ${script} ${param} > ${log} 2>&1

# End of bash script