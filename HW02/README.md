# ECON 35101 International Macroeconomics and Trade Assignment 2
Feng Lin

## Directories
| Path | Description |
|:-----|:-----|
| `./data/`| Data folder. Raw data should be placed here. |
| `./output/`| Output folder. Tables and figures will be saved here. |

## Scripts
- The scripts should be run in the following order.
- Change `dir_proj` in each script to the path where the folders and scripts are in. 

| Path | Description |
|:-----|:-----|
| `./HW02_Table_Stata.do/`| do file that generate results using Stata. |
| `./HW02_Table3_julia.jl/`| jl file that generate results using Julia. |
| `./HW02_Table3_R.R/`| R file that generate results using R. Generate Table 3 with results from the previous two scripts. |

