# ECON 35101 International Macroeconomics and Trade Assignment 1
# Feng Lin

cd("/Users/flin/Dropbox/Chicago/ECON35101_Dingel/HW01/") # Change to the directory where the zip file is unzipped

using Pkg
if false # When running the code for the first time, change to true
    Pkg.add(["Random"])
    Pkg.add(["LinearAlgebra","Optim","BlackBoxOptim"])
    Pkg.add(["DataFrames","DataFramesMeta","Chain","ShiftedArrays"])
    Pkg.add(["Plots","Gadfly","Compose","Cairo","Fontconfig"])
    Pkg.add(["DelimitedFiles"])
    Pkg.instantiate()
end

using Random
using LinearAlgebra,Optim,BlackBoxOptim
using DataFrames,DataFramesMeta,Chain,ShiftedArrays
using Plots,Gadfly,Compose,Cairo,Fontconfig
using DelimitedFiles

a = readdlm("DFS1977_example_a.txt")
b = vec(readdlm("DFS1977_example_b.txt"))
L = [1.0,1.0] # [Foreign,Home], following the order of the a matrix

################################################################################
# Main Solver

function DFS1977solver(a::Array{Float64,2},b::Array{Float64,1},L::Array{Float64,1},g::Float64;p_return::Int64=0,p_opttime::Float64=15.0)
    # a: productivies [foreigh home]
    # b: Cobb-Douglas shares
    # L: country sizes [foreigh,home]
    # g: iceberg trade cost
    # p_return: 0 = return solution; 1 = return main vectors, without solving; 2 = return dataframe, without solving
    # p_opttime: MaxTime allowed for optimizer

    #-----------------------------------------------------------
    # Ensure that inputs are appropriate
    Na,Ca = size(a)
    Nb = length(b)
    N = Na

    A = a[:,1]./a[:,2]
    af = a[:,1]
    ah = a[:,2]
    Lf = L[1]
    Lh = L[2]

    #-------------------
    # a-related check
    Na < 2 ? error("a has less than 2 rows!") : nothing
    sum(a.<0) > 0 ? error("a is not non-negative!") : nothing
    sum((A[1:(Na-1)].-A[2:Na]) .< 0) > 0 ? error("A is not decreasing!") : nothing
    #-------------------
    # b-related check
    Na != Nb ? error("b is of differnt dimension!") : nothing
    sum(b.<=0) > 0 ? error("b is not strictly positive!") : nothing
    abs(1 - sum(b)) > 1e-9 ? error("b does not sum up to 1!") : nothing # Allow for a small error to deal with float issue
    #-------------------
    # g-related check
    ((g > 0.0) & (g <= 1.0)) == false ? error("g is not in (0,1]!") : nothing

    (p_return in [0,1,2]) == false ? error("Wrong p_return!") : nothing

    #-----------------------------------------------------------
    # Generate additional vectors
    Ah = A ./ g # Ah ~ adjusted for home
    Af = A .* g # Af ~ adjusted for foreign
    λh = [iλ > 1 ? 1.0 : iλ for iλ in cumsum(b)] # Fraction of home income spent on home product if z_bar_star = z; Dealing with float issue
    λf = 1 .- λh + b # Fraction of foreign income spent on foreign product if z_bar = z
    B = λh ./ (1 .- λh) .* Lf/Lh

    if p_return == 1 # Do not solve; just return vectors
        return ah,af,A,Ah,Af,λh,λf,B
    elseif p_return == 2 # Do not solve; just return dataframe
        df_in = DataFrame(zi=(1:N),z=(1:N)/N,  A=A,Ah=Ah,Af=Af,  b=b,λh=λh,λf=λf,  B=B)
        return df_in
    end

    #-----------------------------------------------------------
    srch_rng_lb = maximum([minimum(Ah),minimum(Af)])
    srch_rng_ub = minimum([maximum(Ah),maximum(Af)])
    if srch_rng_lb > srch_rng_ub
        error("Improper Parameters! Ah and Af do not overlap!")
    end
    opt_out = bboptimize(x -> f_obj(x[1],Ah,Af,λh,λf,Lh,Lf); SearchRange = (srch_rng_lb, srch_rng_ub), NumDimensions = 1, MaxTime = p_opttime, TraceMode=:silent) #, Method = :adaptive_de_rand_1_bin

    ω_bar = best_candidate(opt_out)[1]
    z_bar = f_cutoff("h",ω_bar,Ah,Af)
    z_bar_star = f_cutoff("f",ω_bar,Ah,Af)
    A_bar = Ah[z_bar]
    A_bar_star = Af[z_bar_star]

    return z_bar,A_bar,z_bar_star,A_bar_star,ω_bar
end

#-----------------------------------------------------------
# Helper Functions

#-------------------
function f_cutoff(country::String,ω,Ah,Af)
    # Function that returns Ah or Af cutoff
    if country == "h"
        return indexin(0, Ah .>= ω)[1] - 1 # zi <= value returned is produced by home
    elseif country == "f"
        return indexin(1, Af .<= ω)[1] # zi >= value returned is produced by foreign
    else
        error("Wrong Specification!")
    end
end

#-------------------
function f_obj(ω,Ah,Af,λh,λf,Lh,Lf) # Ah,Af,λh,λf,Lh,Lf
    # Objective function

    # Find the set of goods
    zih_bar = f_cutoff("h",ω,Ah,Af)
    zif_bar = f_cutoff("f",ω,Ah,Af)
    # Find λh λf
    λh_bar = λh[zih_bar]
    λf_bar = λf[zif_bar]
    # Calculate error
    error = ((1-λh_bar)*ω*Lh - (1-λf_bar)*Lf)^2
    return error
end

#-----------------------------------------------------------
# Run example numbers

z_bar,A_bar,z_bar_star,A_bar_star,ω_bar = DFS1977solver(a,b,[1.0,1.0],1.0);
println("g = 1.0")
println("Solution: z_bar_star = ", z_bar_star, " z_bar = ", z_bar, " ω_bar = ", ω_bar)
println("")

z_bar,A_bar,z_bar_star,A_bar_star,ω_bar = DFS1977solver(a,b,[1.0,1.0],0.9);
println("g = 0.9")
println("Solution: z_bar_star = ", z_bar_star, " z_bar = ", z_bar, " ω_bar = ", ω_bar)
println("")

################################################################################
# Function that produces DFS figure 1

function DFS1977fig1(a::Array{Float64,2},b::Array{Float64,1},L::Array{Float64,1}=[1.0,1.0])
    # In figure 1, trade cost g = 1
    
    df_fig = DFS1977solver(a,b,L,1.0;p_return=2)

    Gadfly.plot(df_fig,
        layer(x=:zi, y=:A, Geom.line,Theme(default_color="red")),
        layer(x=:zi, y=:B, Geom.line),
        Coord.cartesian(ymin=0, ymax=minimum([maximum(df_fig.A),maximum(df_fig.B)])*1.2),
        Guide.manual_color_key("", ["A(z)", "B(z,L*/L)"], ["red", "deepskyblue"]),
        Theme(key_position=:right,key_label_font_size=12pt),
        Guide.ylabel("ω"))
end

plot_fig1 = DFS1977fig1(a,b)
draw(PDF("fig1.pdf"), plot_fig1)

################################################################################
# Gains from Trade
# Here we want to calculate \int b(z)\ln p(z) dz.
# We normalize home wage to 1

function DFS1977gains(a,b,L,g)
    ah,af,A,Ah,Af,λh,λf,B = DFS1977solver(a,b,L,g,p_return=1)
    z_bar,A_bar,z_bar_star,A_bar_star,ω_bar = DFS1977solver(a,b,L,g)

    #-----------------------------------------------------------
    # Autarky
    Uh_autarky = - sum(b .* log.(ah))
    Uf_autarky = - sum(b .* log.(af)) # Mechancially the same

    #-----------------------------------------------------------
    # Trade
    w = 1
    w_star = w / ω_bar
    # Home utility with trade
    zhh = 1:z_bar
    zhf = z_bar+1:length(A)
    Uh_trade = log(w) - ( sum(b[zhh] .* log.(ah[zhh] .* w)) + sum(b[zhf] .* log.(af[zhf] .* w_star ./ g)) )
    # Foreign utility with trade
    zfh = 1:z_bar_star-1
    zff = z_bar_star:length(A)
    Uf_trade = log(w_star) - (sum(b[zfh] .* log.(ah[zfh] .* w ./ g)) + sum(b[zff] .* log.(af[zff] .* w_star)))

    Uh_gains = Uh_trade - Uh_autarky
    Uf_gains = Uf_trade - Uf_autarky

    return Uh_autarky,Uh_trade,Uh_gains,Uf_autarky,Uf_trade,Uf_gains
end

function f_gains_print(array_gains,country::String;ndig::Int64=4)
    array_gains = round.(array_gains;digits=ndig)
    if country == "h"
        println("Home: Autarky: ",array_gains[1],"; Trade: ",array_gains[2],"; Gains: ", array_gains[3])
    elseif country == "f"
        println("Foreign: Autarky: ",array_gains[4],"; Trade: ",array_gains[5],"; Gains: ", array_gains[6])
    else
        error("Wrong Specification!")
    end
end

mtx_gains = zeros(4,6);

#-------------------
# Welfare gains for our examples
# g = 1
mtx_gains[1,:].=DFS1977gains(a,b,L,1.0);
println("g = 1.0")
f_gains_print(mtx_gains[1,:],"h")
f_gains_print(mtx_gains[1,:],"f")
println("")
# g = 0.9
mtx_gains[2,:].=DFS1977gains(a,b,L,0.9);
println("g = 0.9")
f_gains_print(mtx_gains[2,:],"h")
f_gains_print(mtx_gains[2,:],"f")
println("")

#-------------------
# Welfare increases with Foreign uniform technical progress
mtx_gains[3,:].=DFS1977gains(hcat(a[:,1].*0.75,a[:,2]),b,L,0.9); # Foreign uniform tech progress 0.75
println("g = 0.9; UTP 0.75")
f_gains_print(mtx_gains[3,:],"h")
f_gains_print(mtx_gains[3,:],"f")
println("H Diff = ",mtx_gains[3,2] - mtx_gains[2,2]) # Home
println("F Diff = ",mtx_gains[3,5] - mtx_gains[2,5]) # Foreign
println("")
mtx_gains[4,:].=DFS1977gains(hcat(a[:,1].*0.5,a[:,2]),b,L,0.9); # Foreign uniform tech progress 0.5
println("g = 0.9; UTP 0.5")
f_gains_print(mtx_gains[4,:],"h")
f_gains_print(mtx_gains[4,:],"f")
println("H Diff = ",mtx_gains[4,2] - mtx_gains[2,2]) # Home
println("F Diff = ",mtx_gains[4,5] - mtx_gains[2,5]) # Foreign
println("")


################################################################################
# Volume of Trade

# Function that returns trade volume given primitive parameters and specification
function f_trade_vol(z_bar::Int64,z_bar_star::Int64,ω_bar::Float64,a,b,L,g)
    trade_vol = L[1]/ω_bar * sum(b[1:z_bar_star-1]) + L[2] * sum(b[z_bar+1:length(b)])
    return trade_vol
end

# Function that returns selected useful statistics
function f_stats(a::Array{Float64,2},b::Array{Float64,1},L::Array{Float64,1},g::Float64,mtx_in::Matrix{Float64}=zeros(1,4))

    z_bar,A_bar,z_bar_star,A_bar_star,ω_bar = DFS1977solver(a,b,L,g);
    trade_vol = f_trade_vol(z_bar,z_bar_star,ω_bar,a,b,L,g)
    if mtx_in == zeros(1,4)
        mtx_out = vcat([z_bar,z_bar_star,ω_bar,trade_vol]')
    else
        mtx_out = vcat(mtx_in,[z_bar,z_bar_star,ω_bar,trade_vol]')
    end
    return mtx_out
end

# Function that returns squared difference between trade volume and a given value
function f_err(a::Array{Float64,2},b::Array{Float64,1},L::Array{Float64,1},g::Float64,target_vol::Float64)
    # Make sure that a and b are compliant to our requirements
    a = a[sortperm(a[:, 2]), :]
    b = b ./ sum(b)
    mtx_out = f_stats(a,b,L,g)
    return (mtx_out[1,4]-target_vol)^2
end

# Function that compare the results with original
function f_comp(p_str,a,b,L,g,mtx_out,x::Union{Array{Float64,1},Nothing}=nothing,ndig::Int64=4)
    println(p_str, round.(f_stats(a,b,L,g);digits=ndig))
    println("Original: ", round.(mtx_out;digits=ndig))
    if !(x === nothing)
        println("x = ", round.(x;digits=ndig))
    end

    println("Welfare:")
    mtx_gains[1,:].=DFS1977gains(a,b,L,g);
    f_gains_print(mtx_gains[1,:],"h")
    f_gains_print(mtx_gains[1,:],"f")
    println("")
end

mtx_out = f_stats(a,b,[1.0,1.0],0.9)
println(mtx_out)

#-----------------------------------------------------------
# Manipulation 1: Varying only b

# 1.1
# We now find b that yields the same trade volume as before (maximixing over the whole b vector)
Random.seed!(606371)
opt_out = bboptimize(x -> f_err(a,x,[1.0,1.0],0.9,mtx_out[1,4]); SearchRange = (0,2), NumDimensions = length(b), MaxTime = 45.0, TraceMode=:silent);
b1 = best_candidate(opt_out);
b1 = b1./sum(b1);

# Compare the results
f_comp("1.1: ",a,b1,[1.0,1.0],0.9,mtx_out)

# 1.2
# We now find another b that yields the same trade volume as before (changing the first half and second half separately)
Random.seed!(606372)
function f_vary_b(x,b)
    zhalf = floor(Int64,length(b)/2)
    return vcat(x[1].*b[1:zhalf],x[2].*(b[zhalf+1:length(b)].+0.1))
end
opt_out = bboptimize(x -> f_err(a,f_vary_b(x,b),[1.0,1.0],0.9,mtx_out[1,4]); SearchRange = (0,2), NumDimensions = 2, MaxTime = 45.0, TraceMode=:silent);
x = best_candidate(opt_out);
b2 = f_vary_b(x,b);
b2 = b2./sum(b2);
x12 = x

# Compare the results
f_comp("1.2: ",a,b2,[1.0,1.0],0.9,mtx_out,x12)


#-----------------------------------------------------------
# Manipulation 2: Varying only a

# 2.1
# We now find a[:,2] that yields the same trade volume as before (maximixing over the whole a[:,2] vector)
Random.seed!(606377)
opt_out = bboptimize(x -> f_err(hcat(a[:,1],x),b,[1.0,1.0],0.9,mtx_out[1,4]); SearchRange = (0,2), NumDimensions = size(a)[1], MaxTime = 60.0, TraceMode=:silent);
x = best_candidate(opt_out);
a1 = hcat(a[:,1],x);
a1 = a1[sortperm(a1[:, 2]), :];
x21 = x

# Compare the results
f_comp("2.1: ",a1,b,[1.0,1.0],0.9,mtx_out)

# 2.2
# We now find another a that yields the same trade volume as before (changing the first half and second half separately)
Random.seed!(606374)
function f_vary_a(x,a)
    zhalf = floor(Int64,size(a)[1]/2)
    return hcat(a[:,1],vcat(x[1].*a[1:zhalf,2], x[2].*(a[zhalf+1:size(a)[1],2].+0.1)))
end
opt_out = bboptimize(x -> f_err(f_vary_a(x,a),b,[1.0,1.0],0.9,mtx_out[1,4]); SearchRange = (0,2), NumDimensions = 2, MaxTime = 45.0, TraceMode=:silent);
x = best_candidate(opt_out);
a2 = f_vary_a(x,a);
a2 = a2[sortperm(a2[:, 2]), :];
x22 = x

# Compare the results
f_comp("2.2: ",a2,b,[1.0,1.0],0.9,mtx_out,x22)

#-----------------------------------------------------------
# Manipulation 3: Varying both a and b

Random.seed!(606375)

opt_out = bboptimize(x -> f_err(f_vary_a(x[1:2],a),f_vary_b(x[3:4],b),[1.0,1.0],0.9,mtx_out[1,4]); SearchRange = (0,2), NumDimensions = 4, MaxTime = 45.0, TraceMode=:silent);
x = best_candidate(opt_out);
a3 = f_vary_a(x[1:2],a);
a3 = a3[sortperm(a3[:, 2]), :];
b3 = f_vary_b(x[3:4],b);
b3 = b3./sum(b3);
x3 = x

# Compare the results
f_comp("3: ",a3,b3,[1.0,1.0],0.9,mtx_out,x3)

# End of Julia script